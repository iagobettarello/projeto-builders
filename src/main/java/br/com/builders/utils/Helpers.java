package br.com.builders.utils;

import java.util.LinkedHashMap;
import java.util.Map;

public class Helpers {

    public static Map<String, String> getOperador(Map.Entry<String, String> entry, Map<String, String> parametros) {
        String[] chaves = entry.getKey().split("_");
        String operador = null;

        if (chaves.length > 1 && chaves[1] != null) {
            operador = chaves[1];
        }

        /* Monta o mapa com os valores que são utilizados para o filtro */
        Map<String, String> result = new LinkedHashMap<>();
        result.put("chave", chaves[0]);
        result.put("valor", entry.getValue());
        result.put("operador", operador);

        /* Atualiza a chave no mapa dos parametros, removendo os sufixos (se existir) */
        parametros.remove(entry.getKey());
        parametros.put(result.get("chave"), result.get("valor"));

        return result;
    }
}
