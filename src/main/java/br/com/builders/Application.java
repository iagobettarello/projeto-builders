package br.com.builders;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(
		title = "API Clientes",
		version = "1.0.0",
		contact = @Contact(
				name = "Iago Bettarello",
				email = "bettarello.iago@gmail.com",
				url = ""
		)
))
public class Application {

	public static void main(String[] args) {
	SpringApplication.run(Application.class, args);
	}

}
