package br.com.builders.business.impl;

import br.com.builders.business.ClienteBusiness;
import br.com.builders.model.Cliente;
import br.com.builders.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteBusinesImpl implements ClienteBusiness {

    private ClienteRepository cr;

    @Autowired
    public ClienteBusinesImpl(ClienteRepository cr) {
        this.cr = cr;
    }

    @Override
    public List<Cliente> findAll(Pageable pageable) {
        return cr.findAll(pageable).getContent();
    }

    @Override
    public List<Cliente> findAll(Specification<Cliente> spec, Pageable pageable) {
        return cr.findAll(spec, pageable).getContent();
    }

    @Override
    public boolean existsById(Integer id) {
        return cr.existsById(id);
    }

    @Override
    public Cliente find(Integer id) {
        return cr.findById(id).get();
    }

    @Override
    public Cliente create(Cliente cliente) {
        return cr.save(cliente);
    }

    @Override
    public Cliente update(Cliente cliente) {
        return cr.save(cliente);
    }

    @Override
    public Cliente partial(Cliente cliente) {
        return cr.save(cliente);
    }

    @Override
    public void delete(Integer id) {
        cr.deleteById(id);
    }
}
