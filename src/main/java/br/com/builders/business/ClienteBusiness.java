package br.com.builders.business;

import br.com.builders.model.Cliente;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface ClienteBusiness {

    List<Cliente> findAll(Pageable pageable);
    List<Cliente> findAll(Specification<Cliente> spec, Pageable pageable);
    boolean existsById(Integer id);
    Cliente find(Integer id);
    Cliente create(Cliente cliente);
    Cliente update(Cliente cliente);
    Cliente partial(Cliente cliente);
    void delete(Integer id);
}
