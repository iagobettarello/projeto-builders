package br.com.builders.specification;

import org.springframework.data.jpa.domain.Specification;

import java.util.Arrays;
import java.util.List;

public interface Filtro<T> {

    default Specification<T> addCondicao(String key, String value, String operador) {
        if (value == null) {
            return null;
        } else {
            if (operador == null) {
                return (root, query, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get(key)), value.toLowerCase());
            } else if (operador.equalsIgnoreCase("in")) {
                String[] lista = value.toLowerCase().split(",");
                List<String> array = Arrays.asList(lista);
                return (root, query, criteriaBuilder) -> criteriaBuilder.lower(root.get(key)).in(array);
            } else if (operador.equalsIgnoreCase("contains")) {
                return (root, query, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder.lower(root.get(key)), "%" + value.toLowerCase() + "%");
            } else if (operador.equalsIgnoreCase("greater")) {
                return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(key), value);
            } else if (operador.equalsIgnoreCase("lesser")) {
                return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(key), value);
            }

            return null;
        }
    }
}
