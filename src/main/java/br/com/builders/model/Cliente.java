package br.com.builders.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cliente {

    @Id
    @GeneratedValue
    private Integer id;

    @NotEmpty(message = "O campo 'nome' não pode ser vazio!")
    @Size(min = 3, max = 200, message = "O campo 'Nome' deve conter entre 3 e 200 caracteres!")
    private String nome;

    @NotEmpty(message = "O campo 'cpf' não pode ser vazio!")
    @CPF(message = "O CPF informado é inválido!")
    private String cpf;

    @NotNull(message = "O campo 'dataNascimento' não pode ser vazio!")
    @Past(message = "O campo 'dataNascimento' deve ser anterior a data atual!")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dataNascimento;

    @Transient
    private int idade;

    public int getIdade() {
        Period diff = Period.between(this.getDataNascimento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

        return diff.getYears();
    }
}
