package br.com.builders.controller;

import br.com.builders.business.ClienteBusiness;
import br.com.builders.exception.Response;
import br.com.builders.exception.impl.CustomNotFoundException;
import br.com.builders.model.Cliente;
import br.com.builders.specification.impl.ClienteSpecification;
import br.com.builders.utils.Helpers;
import ch.qos.logback.core.net.server.Client;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/clientes")
@Tag(name = "Clientes", description = "API de Clientes")
public class ClienteController {

    private ClienteBusiness cb;
    private ClienteSpecification cs;

    @Autowired
    public ClienteController(ClienteBusiness cb, ClienteSpecification cs) {
        this.cb = cb;
        this.cs = cs;
    }

    @Operation(summary = "Recupera todos os Clientes cadastrados", description = "Você poderá utilizar parametros para" +
            " filtrar o resultado (listados na seção abaixo). Você poderá utilizar os seguintes sufixos para otimizar" +
            " sua consulta (opcionais): \n - **_in:** Verifica se contém alguns dos valores informados para o campo " +
            "selecionado (separados por vírgula e sem espaços). **Ex.:** id_in=1,2,3,4 \n - " +
            "**_contains:** Verifica se o valor enviado aparece em todo ou parte de uma valor no campo selecionado. " +
            "**Ex.:** nome_contains=fulano \n - **_greater:** Verifica se o valor enviado é maior ou igual a algum " +
            "valor no campo selecionado. **Ex.:** salario_greater=1000 \n - **_lesser:** Verifica se o valor enviado" +
            " é menor ou igual a algum valor no campo selecionado. **Ex.:** salario_lesser=1000", tags =
            { "Clientes" }, parameters = {
            @Parameter(name = "id", in = ParameterIn.QUERY),
            @Parameter(name = "nome", in = ParameterIn.QUERY),
            @Parameter(name = "cpf", in = ParameterIn.QUERY),
            @Parameter(name = "dataNascimento", in = ParameterIn.QUERY),
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(array =
            @ArraySchema(schema =  @Schema(implementation = Cliente.class)))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json"))
    })
    @GetMapping(produces = "application/json")
    // ResponseEntity representa a response completa do HTTP: status code, headers, and body
    public ResponseEntity<List<Cliente>> getClientes(@RequestParam(required = false) final Map<String, String> parametros,
                                                   @PageableDefault(size = 10) Pageable pageable) {

        if (parametros.isEmpty() != true) {
            Map.Entry<String, String> primeiro_registro = parametros.entrySet().iterator().next();
            Map<String, String> condicao = Helpers.getOperador(primeiro_registro, parametros);

            Specification<Cliente> spec = Specification.where(cs.addCondicao(condicao.get("chave"), condicao.get("valor"), condicao.get("operador")));
            parametros.remove(condicao.get("chave"));

            for (Map.Entry<String, String> entry : parametros.entrySet()) {
                condicao = Helpers.getOperador(entry, parametros);
                spec.and(cs.addCondicao(condicao.get("chave"), condicao.get("valor"), condicao.get("operador")));
            }

            return new ResponseEntity<List<Cliente>>(cb.findAll(spec, pageable), HttpStatus.OK);
        } else {
            return new ResponseEntity<List<Cliente>>(cb.findAll(pageable), HttpStatus.OK);
        }
    }

    @Operation(summary = "Recupera um Cliente pelo ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema =
            @Schema(implementation = Cliente.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json"))
    })
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Cliente> getClientes(@PathVariable Integer id) {

        if (cb.existsById(id) == false) {
            throw new CustomNotFoundException("Cliente não encontrado!");
        }

        return new ResponseEntity<Cliente>(cb.find(id), HttpStatus.OK);
    }

    @Operation(summary = "Insere um novo Cliente")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created", content = @Content(schema =
            @Schema(implementation = Cliente.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json"))
    })
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Cliente> postClientes(@Parameter(required = true) @Valid @RequestBody Cliente c) {

        if (c.getCpf() != null) {
            c.setCpf(c.getCpf().replaceAll("[\\D]", ""));
        }

        Cliente cliente = cb.create(c);

        return new ResponseEntity<Cliente>(cliente, HttpStatus.CREATED);
    }

    @Operation(summary = "Atualiza um Cliente")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema =
            @Schema(implementation = Cliente.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json"))
    })
    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Cliente> putClientes(@Parameter(required = true) @PathVariable Integer id,
                                             @Parameter(in = ParameterIn.PATH, required = true) @Valid @RequestBody Cliente c) {
        if (cb.existsById(id) == false) {
            throw new CustomNotFoundException("Cliente não encontrado!");
        }

        Cliente cliente = cb.find(id);

        if (c.getCpf() != null) {
            cliente.setCpf(c.getCpf().replaceAll("[\\D]", ""));
        }

        cliente.setDataNascimento(c.getDataNascimento());
        cliente.setNome(c.getNome());

        cb.update(cliente);

        return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
    }

    @Operation(summary = "Atualiza um Cliente Parcialmente")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema =
            @Schema(implementation = Cliente.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json"))
    })
    @PatchMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Cliente> patchClientes(@Parameter(required = true) @PathVariable Integer id,
                                             @Parameter(in = ParameterIn.PATH, required = true) @RequestBody Cliente c) {
        if (cb.existsById(id) == false) {
            throw new CustomNotFoundException("Cliente não encontrado!");
        }

        Cliente cliente = cb.find(id);

        if (c.getCpf() != null) {
            cliente.setCpf(c.getCpf().replaceAll("[\\D]", ""));
        }

        if (c.getDataNascimento() != null) {
            cliente.setDataNascimento(c.getDataNascimento());
        }

        if (c.getNome() != null) {
            cliente.setNome(c.getNome());
        }

        cb.partial(cliente);

        return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
    }

    @Operation(summary = "Remove um Cliente")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "No Content", content = @Content(schema =
            @Schema(implementation = Void.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json")),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema =
            @Schema(implementation = Response.class), mediaType = "application/json"))
    })
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Cliente> deleteClientes(@PathVariable Integer id) {
        if (cb.existsById(id) == false) {
            throw new CustomNotFoundException("Cliente não encontrado!");
        }

        cb.delete(id);
        return new ResponseEntity<Cliente>(new Cliente(), HttpStatus.NO_CONTENT);
    }
}

