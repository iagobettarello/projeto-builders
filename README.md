# Passos para execução do projeto #

Os seguintes programas devem estar instalados na máquina que executará os testes, para que o projeto possa rodar adequadamente:


* Docker Compose
* Maven

OBS.: Os comandos abaixo devem ser executados com permissão de administrador

### 1) Em uma janela do terminal execute o seguinte comando: ###


```sh
$ docker-compose up
```


### 2) Em uma outra janela do terminal execute o seguinte comando: ###


Linux:


```sh
$ mvn spring-boot:run
```


Windows:


```sh
$ mvnw spring-boot:run
```


### 3) Após os comandos executados, poderá utilizar o arquivo do Postman para apreciação das APIs ###
### 4) A documentação da API poderá ser acessada através do link: localhost:8080/v1/api-docs ###